require "./Server/*"
require "uuid"
require "kemal"

module Server
    socks = {} of UUID => HTTP::WebSocket

    p = UpdatePacket.new
    p.data = Array.new(6) do |block|
        Array.new(17) do |b|
            "#ff00ff"
        end
    end

    get "/" do |env|
        render "src/views/index.ecr"
    end

    ws "/socket" do |socket|
        uuid = UUID.random
        socks[uuid] = socket
        socket.send p.to_json

        socket.on_message do |message|
            if message != "ping"
                cp = ColorPacket.from_json message
                p.data[cp.data.key[0]][cp.data.key[1]] = cp.data.val
                socks.each_value do |s|
                    s.send p.to_json
                end
            end
        end

        socket.on_close do
            socks.delete(uuid)
        end
    end

    Kemal.run ENV.fetch("CMK_PORT", "3000").to_i
end
