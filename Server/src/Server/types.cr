require "json"

KEY_MAP = [ 
    [   
        "ESC", "_", "F1", "F2", "F3", "F4", "F5", "F6", "F7", "F8", "F9", "F10",
        "F11", "F12", "PRTSCR", "SCRLK", "PAUSE"
    ],  
    [   
        "&#96;", "1", "2", "3", "4", "5", "6", "7", "8", "9", "0", "-", "=",
        "BACKSPACE", "INS", "HOME", "PAGEUP"
    ],  
    [   
        "TAB", "Q", "W", "E", "R", "T", "Y", "U", "I", "O", "P", "[", "]", "\\",
        "DELETE", "END", "PAGEDOWN"
    ],  
    [   
        "CAPSLK", "A", "S", "D", "F", "G", "H", "J", "K", "L", "&#59;", "&#39;", "_",
        "RETURN", "_", "_", "_" 
    ],  
    [   
        "LSHIFT", "_", "Z", "X", "C", "V", "B", "N", "M", ",", ".", "/", "_",
        "RSHIFT", "_", "UP", "_"
    ],  
    [   
        "LCTRL", "SUPER", "LALT", "_", "_", "_", "SPACE", "_", "_", "_", "RALT",
        "FN", "MENU", "RCTRL", "LEFT", "DOWN", "RIGHT"
    ]   
]; 

class UpdatePacket
    def initialize()
        @type = "update"
        @data = KEY_MAP
    end

    JSON.mapping(
        type: String,
        data: Array(Array(String))
    )
end

class ColorData
    JSON.mapping(
        key: Tuple(Int32, Int32),
        val: String
    )
end

class ColorPacket
    JSON.mapping(
        type: String,
        data: ColorData
    )
end
